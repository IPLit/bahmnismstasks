/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.bahmnisms.api.impl;

import java.util.Date;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.openmrs.Location;
import org.openmrs.LocationAttribute;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.Visit;
import org.openmrs.VisitAttribute;

import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.module.bahmnisms.api.BahmnismsService;
import org.openmrs.module.bahmnisms.api.dao.BahmnismsDao;
import org.openmrs.util.OpenmrsUtil;

// @Service("BahmnismsServiceImpl")
public class BahmnismsServiceImpl extends BaseOpenmrsService implements BahmnismsService {
	
	private BahmnismsDao dao;
	
	/** Logger for this class and subclasses */
	private Logger log = Logger.getLogger(BahmnismsServiceImpl.class);
	
	private static String smsUrl = "";
	
	private static String smsUsername = "";
	
	private static String smsPassword = "";
	
	private static String smsPriorFollowupDate = "";
	
	private static String smsMessage = "Dear %s, Your appointment is scheduled on %s, at %s %s %s.";
	
	// "Dear {0}, Your appointment is scheduled on {1}";
	
	private static Properties properties = null;
	
	private static String followupConceptName = "OPD Follow Up";
	
	public BahmnismsServiceImpl() {
	}
	
	/**
	 * Injected in moduleApplicationContext.xml
	 */
	public void setDao(BahmnismsDao dao) {
		this.dao = dao;
	}
	
	private void init() {
		try {
			if (smsUrl.isEmpty()) {
				load();
				smsUrl = getProperty("smsUrl");
				smsUsername = getProperty("smsUsername");
				smsPassword = getProperty("smsPassword");
				smsPriorFollowupDate = getProperty("smsPriorFollowupDate");
				String smsMessageText = getProperty("smsMessage");
				if (StringUtils.isNotEmpty(smsMessageText)) {
					smsMessage = smsMessageText;
				}
			}
		}
		catch (Exception ex) {
			log.error("Bahmni SMS init error: " + ex.getMessage());
		}
	}
	
	private void load() {
		try {
			File propertyFile = new File(OpenmrsUtil.getApplicationDataDirectory(), "sms.properties");
			properties = new Properties(System.getProperties());
			if (propertyFile.exists()) {
				properties.load(new FileInputStream(propertyFile.getAbsolutePath()));
			}
		}
		catch (IOException e) {
			log.error("Bahmni SMS init load error: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	private String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	private void sendSMS(String recipientMobileNumber, String message) {
		try {
			String recipient = recipientMobileNumber;
			if (message.isEmpty()) {
				message = "Greetings! Have a nice day!";
			}
			
			String requestUrl = smsUrl + "&username=" + URLEncoder.encode(smsUsername, "UTF-8") + "&password="
			        + URLEncoder.encode(smsPassword, "UTF-8") + "&destination=" + URLEncoder.encode(recipient, "UTF-8")
			        + "&message=" + URLEncoder.encode(message, "UTF-8");
			URL url = new URL(requestUrl);
			HttpURLConnection uc = (HttpURLConnection) url.openConnection();
			String respUc = uc.getResponseMessage();
			uc.disconnect();
			//log.debug("SMS sent to patient successfully!");
		}
		catch (Exception ex) {
			log.error("Bahmni SMS send error: " + ex.getMessage());
			System.out.println(ex.getMessage());
		}
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void processFollowupVisitPatients(PatientService patientService) {
		init();
		
		List<Patient> allPatients = patientService.getAllPatients(false);
		int priorDays = 0;
		if (StringUtils.isNotEmpty(smsPriorFollowupDate)) {
			priorDays = Integer.parseInt(smsPriorFollowupDate);
		}
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
		if (allPatients != null && allPatients.size() > 0) {
			for (Patient patient : allPatients) {
				try {
					String locPhoneNo = "";
					Visit visit = dao.getLatestVisit(patient.getUuid());
					Location location = visit != null ? visit.getLocation() : null;
					if (location != null) {
						Collection<LocationAttribute> locAttrs = location.getActiveAttributes();
						if (locAttrs != null) {
							for (LocationAttribute attrLoc : locAttrs) {
								if (attrLoc.getDescriptor().getName().equalsIgnoreCase("LocationOrgPhone")) {
									locPhoneNo = attrLoc.getValue().toString();
									if (StringUtils.isNotEmpty(locPhoneNo)) {
										locPhoneNo = "Phone: " + locPhoneNo;
									}
									break;
								}
							}
						}
					}
					if (visit != null && location != null) {
						Collection<VisitAttribute> attrCollection = visit.getActiveAttributes();
						if (attrCollection != null) {
							for (VisitAttribute attr : attrCollection) {
								if (attr.getAttributeType().getName().equalsIgnoreCase("Follow UP Date")) {
									String followUp = attr.getValue().toString();
									if (StringUtils.isNotEmpty(followUp)) {
										followUp = followUp.replaceAll("/", "-");
										Date followUpDate = fmt.parse(followUp);
										if (followUpDate != null) {
											Calendar cal = Calendar.getInstance();
											Date currDay = new Date();
											cal.setTime(currDay);
											cal.add(Calendar.DATE, priorDays);
											cal.set(Calendar.HOUR, 0);
											cal.set(Calendar.MINUTE, 0);
											cal.set(Calendar.SECOND, 0);
											cal.set(Calendar.HOUR_OF_DAY, 0);
											cal.set(Calendar.MILLISECOND, 0);
											Date smsToSendDate = cal.getTime();
											long smsDateTime = smsToSendDate.getTime();
											followUpDate.setHours(0);
											followUpDate.setMinutes(0);
											followUpDate.setSeconds(0);
											long followupDateTime = followUpDate.getTime();
											if (followupDateTime == smsDateTime) {
												List<PersonAttribute> pattrs = patient.getActiveAttributes();
												if (pattrs != null) {
													for (PersonAttribute personAttribute : pattrs) {
														if (personAttribute.getAttributeType().toString()
														        .equalsIgnoreCase("mobileNo")) {
															String patientMobileNumber = personAttribute.getValue();
															if (StringUtils.isNotEmpty(patientMobileNumber)) {
																String patientFullName = patient.getPersonName()
																        .getFullName();
																String message = String.format(smsMessage, patientFullName,
																    fmt.format(followUpDate), location.getAddress1(),
																    location.getCityVillage(), locPhoneNo);
																sendSMS(patientMobileNumber.trim(), message);
															}
															break;
														}
													}
												}
											}
										}
									}
									break;
								}
							}
						}
					}
				}
				catch (Exception ex) {
					log.error("Error sending SMS in processFollowupVisitPatients: " + ex.getMessage());
				}
			}
		}
	}
	
	/*	public void processFollowupVisitPatients(PatientService patientService) {
			init();
			
			List<Patient> allPatients = patientService.getAllPatients(false);
			int priorDays = 0;
			if (StringUtils.isNotEmpty(smsPriorFollowupDate)) {
				priorDays = Integer.parseInt(smsPriorFollowupDate);
			}
			SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
			if (allPatients != null && allPatients.size() > 0) {
				List<Location> allLocs = Context.getLocationService().getAllLocations(false);
				for (Patient patient : allPatients) {
					try {
						Location location = null;
						String locPhoneNo = "";
						String pIdentifier = patient.getPatientIdentifier().getIdentifier();
						Visit visit = null;
						for (Location loc : allLocs) {
							if (pIdentifier.startsWith(loc.getName())) {
								location = loc;
								break;
							}
						}
						if (location != null) {
							visit = dao.getLatestVisit(patient.getUuid());
							Collection<LocationAttribute> locAttrs = location.getActiveAttributes();
							if (locAttrs != null) {
								for (LocationAttribute attrLoc : locAttrs) {
									if (attrLoc.getDescriptor().getName().equalsIgnoreCase("LocationOrgPhone")) {
										locPhoneNo = attrLoc.getValue().toString();
										if (StringUtils.isNotEmpty(locPhoneNo)) {
											locPhoneNo = " Phone: " + locPhoneNo;
										}
										break;
									}
								}
							}
							
						}
						if (visit != null && location != null) {
							Collection<VisitAttribute> attrCollection = visit.getActiveAttributes();
							if (attrCollection != null) {
								for (VisitAttribute attr : attrCollection) {
									if (attr.getAttributeType().getName().equalsIgnoreCase("Follow UP Date")) {
										String followUp = attr.getValue().toString();
										if (StringUtils.isNotEmpty(followUp)) {
											followUp = followUp.replaceAll("/", "-");
											Date followUpDate = fmt.parse(followUp);
											if (followUpDate != null) {
												Calendar cal = Calendar.getInstance();
												Date currDay = new Date();
												cal.setTime(currDay);
												cal.add(Calendar.DATE, priorDays);
												cal.set(Calendar.HOUR, 0);
												cal.set(Calendar.MINUTE, 0);
												cal.set(Calendar.SECOND, 0);
												cal.set(Calendar.HOUR_OF_DAY, 0);
												cal.set(Calendar.MILLISECOND, 0);
												Date smsToSendDate = cal.getTime();
												long smsDateTime = smsToSendDate.getTime();
												
												followUpDate.setHours(0);
												followUpDate.setMinutes(0);
												followUpDate.setSeconds(0);
												long followupDateTime = followUpDate.getTime();
												if (followupDateTime == smsDateTime) {
													List<PersonAttribute> pattrs = patient.getActiveAttributes();
													if (pattrs != null) {
														for (PersonAttribute personAttribute : pattrs) {
															if (personAttribute.getAttributeType().toString()
															        .equalsIgnoreCase("mobileNo")) {
																String patientMobileNumber = personAttribute.getValue();
																
																if (StringUtils.isNotEmpty(patientMobileNumber)) {
																	String patientFullName = patient.getPersonName()
																	        .getFullName();
																	// + " " + patient.getFamilyName() != null ? patient.getFamilyName() : "";
																	String message = String.format(smsMessage, patientFullName,
																	    fmt.format(followUpDate), location.getName(),
																	    location.getAddress1(), location.getCityVillage(),
																	    locPhoneNo);
																	sendSMS(patientMobileNumber, message);
																}
																break;
															}
														}
													}
												}
											}
										}
										break;
									}
								}
							}
						}
					}
					catch (Exception ex) {
						log.error("Error sending SMS in processFollowupPatients: " + ex.getMessage());
					}
				}
			}
		} */
	
	/* 	@SuppressWarnings("deprecation")
		@Override
		public void processFollowupObsPatients(PatientService patientService) {
			init();
		
			List<Patient> allPatients = patientService.getAllPatients(false);
			int priorDays = 0;
			if (StringUtils.isNotEmpty(smsPriorFollowupDate)) {
				priorDays = Integer.parseInt(smsPriorFollowupDate);
			}
			SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
			if (allPatients != null && allPatients.size() > 0) {
				List<Location> allLocs = Context.getLocationService().getAllLocations(false);
				for (Patient patient : allPatients) {
					try {
						Location location = null;
						String locPhoneNo = "";
						String pIdentifier = patient.getPatientIdentifier().getIdentifier();
						Visit visit = null;
						for (Location loc : allLocs) {
							if (pIdentifier.startsWith(loc.getName())) {
								location = loc;
								break;
							}
						}
						if (location != null) {
						
							visit = dao.getLatestVisit(patient.getUuid(), location.getLocationId());
							Collection<LocationAttribute> locAttrs = location.getActiveAttributes();
							if (locAttrs != null) {
								for (LocationAttribute attrLoc : locAttrs) {
									if (attrLoc.getDescriptor().getName().equalsIgnoreCase("LocationOrgPhone")) {
										locPhoneNo = attrLoc.getValue().toString();
										if (StringUtils.isNotEmpty(locPhoneNo)) {
											locPhoneNo = " Phone: " + locPhoneNo;
										}
										break;
									}
								}
							}
						}
						if (visit != null && location != null) {
							Obs followupdateObs = dao.getLatestVisitFolloupDateObs(patient.getUuid(), visit.getVisitId());
							if (followupdateObs != null) {
								String followUp = followupdateObs.getValueAsString(Context.getLocale());
								if (StringUtils.isNotEmpty(followUp)) {
									followUp = followUp.replaceAll("/", "-");
									Date followUpDate = fmt.parse(followUp);
									if (followUpDate != null) {
										Calendar cal = Calendar.getInstance();
										Date currDay = new Date();
										cal.setTime(currDay);
										cal.add(Calendar.DATE, priorDays);
										cal.set(Calendar.HOUR, 0);
										cal.set(Calendar.MINUTE, 0);
										cal.set(Calendar.SECOND, 0);
										cal.set(Calendar.HOUR_OF_DAY, 0);
										cal.set(Calendar.MILLISECOND, 0);
										Date smsToSendDate = cal.getTime();
										long smsDateTime = smsToSendDate.getTime();
										
										followUpDate.setHours(0);
										followUpDate.setMinutes(0);
										followUpDate.setSeconds(0);
										long followupDateTime = followUpDate.getTime();
										if (followupDateTime == smsDateTime) {
											List<PersonAttribute> pattrs = patient.getActiveAttributes();
											if (pattrs != null) {
												for (PersonAttribute personAttribute : pattrs) {
													if (personAttribute.getAttributeType().toString()
													        .equalsIgnoreCase("mobileNo")) {
														String patientMobileNumber = personAttribute.getValue();
														if (StringUtils.isNotEmpty(patientMobileNumber)) {
															String patientFullName = patient.getPersonName()
															        .getFullName();
															// + " " + patient.getFamilyName() != null ? patient.getFamilyName() : "";
															String message = String.format(smsMessage, patientFullName,
															    fmt.format(followUpDate), location.getName(),
															    location.getAddress1(), location.getCityVillage(),
															    locPhoneNo);
															sendSMS(patientMobileNumber, message);
														}
														break;
													}
												}
											}
										}
									}
								}
							}
						}
					}
					catch (Exception ex) {
						log.error("Error sending SMS in processFollowupPatients: " + ex.getMessage());
					}
				}
			}
		} */
	
	@SuppressWarnings("deprecation")
	@Override
	public void processFollowupObsPatients(PatientService patientService) {
		init();
		
		List<Patient> allPatients = patientService.getAllPatients(false);
		int priorDays = 0;
		if (StringUtils.isNotEmpty(smsPriorFollowupDate)) {
			priorDays = Integer.parseInt(smsPriorFollowupDate);
		}
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		if (allPatients != null && allPatients.size() > 0) {
			for (Patient patient : allPatients) {
				try {
					String locPhoneNo = "";
					Visit visit = dao.getLatestVisit(patient.getUuid());
					Location location = visit != null ? visit.getLocation() : null;
					if (location != null) {
						location = Context.getLocationService().getLocation(location.getName());
						Collection<LocationAttribute> locAttrs = location.getActiveAttributes();
						if (locAttrs != null) {
							for (LocationAttribute attrLoc : locAttrs) {
								if (attrLoc.getDescriptor().getName().equalsIgnoreCase("LocationOrgPhone")) {
									locPhoneNo = attrLoc.getValue().toString();
									if (StringUtils.isNotEmpty(locPhoneNo)) {
										locPhoneNo = "Phone: " + locPhoneNo;
									}
									break;
								}
							}
						}
					}
					if (visit != null && location != null) {
						Obs followupdateObs = dao.getLatestObsFor(patient.getUuid(), followupConceptName);
						if (followupdateObs != null) {
							Date followUpDate = followupdateObs.getValueDate();
							if (followUpDate != null) {
								//followUp = followUp.replaceAll("/", "-");
								//Date followUpDate = fmt.parse(followUp);
								//if (followUpDate != null) {
								Calendar cal = Calendar.getInstance();
								Date currDay = new Date();
								cal.setTime(currDay);
								cal.add(Calendar.DATE, priorDays);
								cal.set(Calendar.HOUR, 0);
								cal.set(Calendar.MINUTE, 0);
								cal.set(Calendar.SECOND, 0);
								cal.set(Calendar.HOUR_OF_DAY, 0);
								cal.set(Calendar.MILLISECOND, 0);
								Date smsToSendDate = cal.getTime();
								long smsDateTime = smsToSendDate.getTime();
								followUpDate.setHours(0);
								followUpDate.setMinutes(0);
								followUpDate.setSeconds(0);
								long followupDateTime = followUpDate.getTime();
								if (followupDateTime == smsDateTime) {
									List<PersonAttribute> pattrs = patient.getActiveAttributes();
									if (pattrs != null) {
										for (PersonAttribute personAttribute : pattrs) {
											if (personAttribute.getAttributeType().toString().equalsIgnoreCase("mobileNo")) {
												String patientMobileNumber = personAttribute.getValue();
												if (StringUtils.isNotEmpty(patientMobileNumber)) {
													String patientFullName = patient.getPersonName().getFullName();
													String message = String.format(smsMessage, patientFullName,
													    fmt.format(followUpDate), location.getAddress1(),
													    location.getCityVillage(), locPhoneNo);
													sendSMS(patientMobileNumber.trim(), message);
												}
												break;
											}
										}
									}
								}
								//}
							}
						}
					}
				}
				catch (Exception ex) {
					log.error("Error sending SMS in processFollowupObsPatients: " + ex.getMessage());
				}
			}
		}
	}
	
}
